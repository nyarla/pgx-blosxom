# What is this ?

This package is [Pegex](http://www.pegex.org/) grammer for
[blosxom](http://blosxom.sourceforge.net/) and [plasxom](https://github.com/nyarla/plasxom)(DEPRECATED) file formats.

# Author

Naoki Okamura (Nyarla) *nyarla[ at ]thotep.net*

# License

These grammers are under public domain.
