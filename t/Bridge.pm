package t::Bridge;

use strict;
use warnings;

use File::Spec;
use FindBin;
use Path::Class;

use Pegex;
use YAML::XS;

our $distdir;

{
    my @path = File::Spec->splitdir( File::Spec->rel2abs($FindBin::Bin) );
    while ( defined( my $dir = pop @path ) ) {
        if ( $dir eq 't' ) {
            $distdir = dir(@path)->cleanup;
        }
    }
}

sub parse {
    my $input  = (shift)->value;
    my $format = (shift)->value;

    my $grammer = $distdir->file("${format}.pgx")->slurp();
    my $parser  = pegex($grammer);

    return $parser->parse($input);
}

sub yaml {
    YAML::XS::Load( (shift)->value );
}

1;
