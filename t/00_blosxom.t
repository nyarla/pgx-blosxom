#!/usr/bin/env perl

use strict;
use warnings;

BEGIN {
    $TestML::Test::Differences = 1;
}

use TestML -run,
    -testml => '../tml/blosxom.tml',
    -bridge => 't::Bridge';


1;
