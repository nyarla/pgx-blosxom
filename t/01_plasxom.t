#!/usr/bin/env perl

use strict;
use warnings;

BEGIN {
    $TestML::Test::Differences = 1;
}

use TestML -run,
    -testml => '../tml/plasxom.tml',
    -bridge => 't::Bridge';


1;
