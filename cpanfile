requires 'Pegex' => '>= 0.20';

on test => sub {
   requires 'TestML'            => '>= 0.27';
   requires 'Path::Class'       => '>= 0.26';
   requires 'YAML::XS'          => '>= 0.38';
   requires 'Test::Differences' => '>= 0.61';
};

